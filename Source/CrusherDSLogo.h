/*
  ==============================================================================

    CrusherDSLogo.h
    Created: 11 Sep 2019 10:35:18am
    Author:  Mateusz Salski

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

namespace dsteam
{
    namespace gui
    {
        class CrusherDSLogo : public DigitalSteamLogo
        {
        public:
            
            CrusherDSLogo() : DigitalSteamLogo()
            {
                pluginNameLabel.setColour(Label::ColourIds::textColourId, Colours::black);
                pluginVersionLabel.setColour(Label::ColourIds::textColourId, Colours::black);
            }
            
            void resized() override
            {
                auto digitalSteamLabelArea = getLocalBounds();
                digitalSteamLabelArea.reduce(0, 5);
                
                pluginNameLabel.setBounds(digitalSteamLabelArea.removeFromLeft(digitalSteamLabelArea.getWidth() / 2 ));
                pluginNameLabel.setJustificationType(Justification::topLeft);
                pluginNameLabel.setFont(static_cast<float>(pluginNameLabel.getHeight()));
                
                pluginVersionLabel.setBounds(
                    digitalSteamLabelArea
                        .removeFromLeft(digitalSteamLabelArea.getWidth() / 4)
                        .removeFromBottom(35)
                );
                pluginNameLabel.setJustificationType(Justification::topLeft);
                
                digitalSteamLink.setBounds(digitalSteamLabelArea.removeFromTop(20).removeFromRight(100));
                digitalSteamLink.setJustificationType(Justification::topRight);
            }
        };
    }
}
