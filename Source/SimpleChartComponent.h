/*
  ==============================================================================

    SimpleChartComponent.h
    Created: 9 Sep 2019 6:24:15am
    Author:  Mateusz Salski

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

//==============================================================================
/*
*/
class SimpleChartComponent    : public Component
{
public:
    SimpleChartComponent();
    ~SimpleChartComponent();

    void paint (Graphics&) override;
    void resized() override;
    
    void setData(const float* newData, int newDataSize)
    {
        data = newData;
        dataSize = newDataSize;
    }
    
    void setHAxisRes(float har)
    {
        horizAxisRes = har;
    }
    
    void setVAxisRes(float var)
    {
        vertAxisRes = var;
    }

private:
    const float* data { nullptr };
    int dataSize { 0 };
    
    float horizAxisRes { 0.f };
    float vertAxisRes { 0.f };
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (SimpleChartComponent)
};
