/*
  ==============================================================================

    Crusher.h
    Created: 9 Sep 2019 5:41:40am
    Author:  Mateusz Salski

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

namespace dsteam
{

    class Crusher
    {
    public:
        
        void prepare(const dsp::ProcessSpec& spec)
        {
            holdSamples.resize(spec.numChannels);
        }
        
        void reset()
        {
            holdedSamplesNum = 0.0f;
        }
        
        template<typename ProcessContext>
        void process (const ProcessContext& context) noexcept
        {
            processBlock(context.getInputBlock(), context.getOutputBlock());
        }
        
        void processBlock (AudioBuffer<float>& inBuffer, AudioBuffer<float>& outBuffer)
        {

            for (int channel = 0; channel < holdSamples.size(); ++channel)
            {
                auto* outData = outBuffer.getWritePointer(channel);
                auto* inData = inBuffer.getReadPointer(channel);
                
                processOneChannel(inData, outData, jmin(inBuffer.getNumSamples(), outBuffer.getNumSamples()), holdSamples[channel]);
            }
            
        }
        
        void processOneChannel(const float* inData, float* outData, int numSamples, int channel)
        {
            
            for (int n = 0; n < numSamples; ++n)
            {
                if (holdedSamplesNum == 0)
                    holdSamples[channel] = inData[n];
                
                outData[n] = valueResolutioner(holdSamples[channel], valueResolution);
                
                if ( ++holdedSamplesNum > maxHoldSamplesNum )
                    holdedSamplesNum = 0;
            }
            
        }
        
        void setValueResolution (float newValueResolution)
        {
            valueResolution = newValueResolution;
        }
        
        void setHoldedSamplesNum (int newHoldedSamplesNum)
        {
            maxHoldSamplesNum = newHoldedSamplesNum;
        }
        
    private:
        
        int maxHoldSamplesNum { 8 };
        
        float valueResolution { 0.f };
        int holdedSamplesNum { 0 };
        
        std::vector<float> holdSamples;
        
        float valueResolutioner(float value, float resolution)
        {
            if (resolution == 0.0f || fmod(value, resolution) == 0.f)
                return value;
            
            int i = ( int ) ( value / resolution );
            
            float rangeMin = ( float ) i * resolution;
            float rangeMax = rangeMin + resolution;
            
            return rangeMin + ( rangeMax - rangeMin ) / 2.f;
        }
    };

}
