/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
CrusherAudioProcessor::CrusherAudioProcessor()
#ifndef JucePlugin_PreferredChannelConfigurations
     : AudioProcessor (BusesProperties()
                     #if ! JucePlugin_IsMidiEffect
                      #if ! JucePlugin_IsSynth
                       .withInput  ("Input",  AudioChannelSet::stereo(), true)
                      #endif
                       .withOutput ("Output", AudioChannelSet::stereo(), true)
                     #endif
                       ),
#endif
    state(*this, nullptr, "DSteamCrusher",
          {
              std::make_unique<AudioParameterInt>("numSamples", "Num samples", 0, 100, 8),
              std::make_unique<AudioParameterFloat>("valueResolution", "Value resolution", NormalisableRange<float> { 0.0f, 0.25f, 0.00001f }, 0.0f)
          })
{
    numSamplesParam = dynamic_cast<AudioParameterInt*>(state.getParameter("numSamples"));
    numSamplesParam->addListener(this);
    
    valueResolution = dynamic_cast<AudioParameterFloat*>(state.getParameter("valueResolution"));
    valueResolution->addListener(this);
}

void CrusherAudioProcessor::parameterValueChanged (int parameterIndex, float newValue)
{
    if (parameterIndex == numSamplesParam->getParameterIndex())
    {
        crusher.setHoldedSamplesNum(numSamplesParam->get());
    }
    else if (parameterIndex == valueResolution->getParameterIndex())
    {
        crusher.setValueResolution(valueResolution->get());
    }
}

CrusherAudioProcessor::~CrusherAudioProcessor()
{
}

//==============================================================================
const String CrusherAudioProcessor::getName() const
{
    return JucePlugin_Name;
}

bool CrusherAudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool CrusherAudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

bool CrusherAudioProcessor::isMidiEffect() const
{
   #if JucePlugin_IsMidiEffect
    return true;
   #else
    return false;
   #endif
}

double CrusherAudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int CrusherAudioProcessor::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

int CrusherAudioProcessor::getCurrentProgram()
{
    return 0;
}

void CrusherAudioProcessor::setCurrentProgram (int index)
{
}

const String CrusherAudioProcessor::getProgramName (int index)
{
    return {};
}

void CrusherAudioProcessor::changeProgramName (int index, const String& newName)
{
}

//==============================================================================
void CrusherAudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
    dsp::ProcessSpec spec;
    spec.numChannels = jmax(getTotalNumInputChannels(), getTotalNumOutputChannels());
    spec.sampleRate = sampleRate;
    spec.maximumBlockSize = samplesPerBlock;
    
    crusher.prepare(spec);
}

void CrusherAudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

#ifndef JucePlugin_PreferredChannelConfigurations
bool CrusherAudioProcessor::isBusesLayoutSupported (const BusesLayout&) const
{
    return true;
}
#endif



void CrusherAudioProcessor::processBlock (AudioBuffer<float>& buffer, MidiBuffer& midiMessages)
{
    ScopedNoDenormals noDenormals;
    auto totalNumInputChannels  = getTotalNumInputChannels();
    auto totalNumOutputChannels = getTotalNumOutputChannels();

    for (auto i = totalNumInputChannels; i < totalNumOutputChannels; ++i)
        buffer.clear (i, 0, buffer.getNumSamples());

    crusher.processBlock(buffer, buffer);
}

//==============================================================================
bool CrusherAudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

AudioProcessorEditor* CrusherAudioProcessor::createEditor()
{
    return new CrusherAudioProcessorEditor (*this, state);
}

//==============================================================================
void CrusherAudioProcessor::getStateInformation (MemoryBlock& destData)
{
    // You should use this method to store your parameters in the memory block.
    // You could do that either as raw data, or use the XML or ValueTree classes
    // as intermediaries to make it easy to save and load complex data.
}

void CrusherAudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
    // You should use this method to restore your parameters from this memory block,
    // whose contents will have been created by the getStateInformation() call.
}

//==============================================================================
// This creates new instances of the plugin..
AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new CrusherAudioProcessor();
}
