/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
CrusherAudioProcessorEditor::CrusherAudioProcessorEditor (CrusherAudioProcessor& p, AudioProcessorValueTreeState& napvts)
    : AudioProcessorEditor (&p), processor (p), apvts(napvts)
{
    dsp::ProcessSpec spec;
    spec.numChannels = 1;
    crusher.prepare(spec);
    
    updateData();
    
    chart.setData(data, dataSize);
    
    numSamplesSlider.addListener(this);
    resolutionSlider.addListener(this);
    
    setOpaque(true);
    setSize (400, 400);
    
   // resolutionSlider.setSliderStyle(Slider::SliderStyle::LinearVertical);
    resolutionSlider.setTextBoxStyle(Slider::TextEntryBoxPosition::TextBoxBelow, false, 120, 20);
    
    numSamplesSlider.setTextBoxStyle(Slider::TextEntryBoxPosition::TextBoxAbove, false, 120, 20);
    
    getLookAndFeel().setColour(ResizableWindow::backgroundColourId, Colours::whitesmoke);
    resolutionSlider.setColour(Slider::ColourIds::textBoxTextColourId , Colours::black);
    numSamplesSlider.setColour(Slider::ColourIds::textBoxTextColourId , Colours::black);
    
    addAndMakeVisible(logo);
    addAndMakeVisible(numSamplesSlider);
    addAndMakeVisible(resolutionSlider);
    addAndMakeVisible(chart);
    
}

CrusherAudioProcessorEditor::~CrusherAudioProcessorEditor()
{
    numSamplesSlider.removeListener(this);
    resolutionSlider.removeListener(this);
}

//==============================================================================
void CrusherAudioProcessorEditor::paint (Graphics& g)
{
    g.fillAll (getLookAndFeel().findColour (ResizableWindow::backgroundColourId));
}

void CrusherAudioProcessorEditor::resized()
{
    auto area = getLocalBounds();
    area.reduce(5, 5);
    
    int sliderSize = 80;
    
    logo.setBounds(area.removeFromTop(sliderSize));
    
    auto resSliderAreaNotrotated = area.removeFromLeft(sliderSize).removeFromTop(getHeight() - sliderSize - logo.getHeight());
    resolutionSlider.setBounds(
                               0,
                               0,
                               resSliderAreaNotrotated.getHeight(),
                               resSliderAreaNotrotated.getWidth()
    );
    resolutionSlider.setTransform(
        AffineTransform::rotation(
            -.5f * MathConstants<float>::pi
        )
        .translated(0, resolutionSlider.getWidth() + resSliderAreaNotrotated.getY())
    );
    
    numSamplesSlider.setBounds(area.removeFromBottom(sliderSize).reduced(0, 5));
    
    chart.setBounds(area);
}

void CrusherAudioProcessorEditor::sliderValueChanged (Slider* slider)
{
    if (slider == &numSamplesSlider)
    {
        crusher.setHoldedSamplesNum(numSamplesSlider.getValue());
        chart.setVAxisRes( (float) numSamplesSlider.getValue() / (float) dataSize);
    }
    else if (slider == &resolutionSlider)
    {
        crusher.setValueResolution(resolutionSlider.getValue());
        chart.setHAxisRes(resolutionSlider.getValue());
    }
    
    updateData();
    repaint();
}

void CrusherAudioProcessorEditor::updateData()
{
    float phase = 0.f;
    float phaseStep = MathConstants<float>::twoPi / dataSize;
    for (int i = 0; i < dataSize; ++i)
    {
        data[i] = sin(phase);
        phase += phaseStep;
    }
    
    crusher.reset();
    crusher.processOneChannel(data, data, dataSize, 0);
}
