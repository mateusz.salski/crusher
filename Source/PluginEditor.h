/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "PluginProcessor.h"
#include "SimpleChartComponent.h"
#include "Crusher.h"
#include "CrusherDSLogo.h"

//==============================================================================
/**
*/
class CrusherAudioProcessorEditor  : public AudioProcessorEditor, Slider::Listener
{
public:
    CrusherAudioProcessorEditor (CrusherAudioProcessor&, AudioProcessorValueTreeState&);
    ~CrusherAudioProcessorEditor();

    //==============================================================================
    void paint (Graphics&) override;
    void resized() override;

private:
    CrusherAudioProcessor& processor;
    
    AudioProcessorValueTreeState& apvts;
    
    dsteam::gui::CrusherDSLogo logo;
    dsteam::gui::SliderWithAttachment numSamplesSlider { apvts, "numSamples" };
    dsteam::gui::SliderWithAttachment resolutionSlider { apvts, "valueResolution" };
    SimpleChartComponent chart;
    
    constexpr static int dataSize = 500;
    float data[dataSize];
    dsteam::Crusher crusher;
    
    //------------------------------------------------------------------------------
    
    void updateData();
    
    //==============================================================================
    void sliderValueChanged (Slider* slider) override;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (CrusherAudioProcessorEditor)
};
