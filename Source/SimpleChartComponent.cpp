/*
  ==============================================================================

    SimpleChartComponent.cpp
    Created: 9 Sep 2019 6:24:15am
    Author:  Mateusz Salski

  ==============================================================================
*/

#include "../JuceLibraryCode/JuceHeader.h"
#include "SimpleChartComponent.h"

//==============================================================================
SimpleChartComponent::SimpleChartComponent()
{
    // In your constructor, you should add any child components, and
    // initialise any special settings that your component needs.

}

SimpleChartComponent::~SimpleChartComponent()
{
}

void SimpleChartComponent::paint (Graphics& g)
{
    auto componentArea = getLocalBounds();
    auto h2 = getHeight() / 2;
    auto w2 = getWidth() / 2;
    
    g.setColour(Colours::cadetblue);
    if (horizAxisRes > 0.f)
    {
        for (float ha = 0.f; ha < 1.f; ha += horizAxisRes)
        {
            g.drawHorizontalLine(h2 - ha * h2, 0, getWidth());
            g.drawHorizontalLine(h2 + ha * h2, 0, getWidth());
        }
    }
    else
    {
        g.fillRect(componentArea);
    }
    
    g.setColour(Colours::darkslateblue.withAlpha(0.5f));
    if (vertAxisRes > 0.f)
    {
        for (float va = 0.f; va < 1.f; va += vertAxisRes)
        {
            g.drawVerticalLine(va * getWidth(), 0, getWidth());
            g.drawVerticalLine(va * getWidth(), 0, getWidth());
        }
    }
    else
    {
        g.fillRect(componentArea);
    }
    
    g.setColour(Colours::darkgrey);
    g.drawRect(componentArea);
    g.drawLine(0, h2, getWidth(), h2);
    g.drawLine(w2, 0, w2, getHeight());
    
    if (data == nullptr)
        return;
    
    float pixelStep = (float) getWidth() / (float) dataSize;
    
    Path p;
    p.startNewSubPath(0, h2  - h2 * ( data[0] ));
    
    for (int i = 1; i < dataSize; ++i)
    {
        p.lineTo ( ( float ) i * pixelStep, h2  - h2 * ( data[i] ) );
    }
    
    g.setColour(Colours::black);
    g.strokePath(p, PathStrokeType(2));
    
}

void SimpleChartComponent::resized()
{
    // This method is where you should set the bounds of any child
    // components that your component contains..

}
